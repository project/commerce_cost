<?php

namespace Drupal\Tests\commerce_cost_field\FunctionalJavascript;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\Entity\NodeType;

/**
 * Tests the price with cost widget.
 *
 * @group commerce_cost
 */
class PriceWithCostWidgetTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field_ui',
    'node',
    'commerce_price',
    'commerce_cost_field',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    NodeType::create(['name' => 'Article', 'type' => 'article'])->save();

    // Create fields for storing the default commerce price and a cost field
    // where we are going to use our widget.
    foreach (['price', 'cost'] as $field) {
      $fieldStorage = FieldStorageConfig::create([
        'field_name' => 'field_' . $field,
        'entity_type' => 'node',
        'type' => 'commerce_price',
      ]);
      $fieldStorage->save();

      FieldConfig::create([
        'field_storage' => $fieldStorage,
        'bundle' => 'article',
        'label' => $field,
      ])->save();
    }

    // Setup the form display.
    $entityFormDisplay = EntityFormDisplay::load('node.article.default');
    if (!$entityFormDisplay) {
      $entityFormDisplay = EntityFormDisplay::create([
        'targetEntityType' => 'node',
        'bundle' => 'article',
        'mode' => 'default',
        'status' => TRUE,
      ]);
      $entityFormDisplay->save();
    }

    foreach (['price', 'cost'] as $field) {
      $entityFormDisplay->setComponent('field_' . $field, [
        'type' => 'commerce_price_default',
      ])->save();
    }
  }

  /**
   * Tests the price with cost wigdet settings and calculation.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testWidget() {
    $adminUser = $this->createUser([
      'create article content',
      'administer node form display',
    ]);
    $this->drupalLogin($adminUser);

    $this->drupalGet('/admin/structure/types/manage/article/form-display');
    $this->assertTrue($this->assertSession()->optionExists('fields[field_cost][type]', 'commerce_price_with_cost'));

    $this->getSession()->getPage()->selectFieldOption('fields[field_cost][type]', 'commerce_price_with_cost');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->find('css', '[name="field_cost_settings_edit"]')->click();
    $this->assertSession()->assertWaitOnAjaxRequest();

    $this->getSession()->getPage()->hasSelect('fields[field_cost][settings_edit_form][settings][price_field]', 'Commerce price field settings is available');
    $this->assertSession()->optionExists('fields[field_cost][settings_edit_form][settings][price_field]', 'field_cost');
    $this->assertSession()->optionExists('fields[field_cost][settings_edit_form][settings][price_field]', 'field_price');

    $this->getSession()->getPage()->hasSelect('fields[field_cost][settings_edit_form][settings][cost_field]', 'Cost price field settings is available');
    $this->assertSession()->optionExists('fields[field_cost][settings_edit_form][settings][cost_field]', 'field_cost');
    $this->assertSession()->optionExists('fields[field_cost][settings_edit_form][settings][cost_field]', 'field_price');

    $this->getSession()->getPage()->hasSelect('fields[field_cost][settings_edit_form][settings][calculation_type]', 'Calculation type settings is available');
    $this->assertSession()->optionExists('fields[field_cost][settings_edit_form][settings][calculation_type]', 'markup');
    $this->assertSession()->optionExists('fields[field_cost][settings_edit_form][settings][calculation_type]', 'margin');

    // Setting configuration to test.
    $this->getSession()->getPage()->selectFieldOption('fields[field_cost][settings_edit_form][settings][price_field]', 'field_price');
    $this->getSession()->getPage()->selectFieldOption('fields[field_cost][settings_edit_form][settings][cost_field]', 'field_cost');
    $this->getSession()->getPage()->selectFieldOption('fields[field_cost][settings_edit_form][settings][calculation_type]', 'markup');
    $this->getSession()->getPage()->pressButton('Update');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->submitForm([], 'Save');
    $this->assertSession()->pageTextContains('Your settings have been saved.');

    // Testing widget with markup.
    $this->drupalGet('node/add/article');
    $this->assertSession()->pageTextContainsOnce('Markup');
    $this->getSession()->getPage()->fillField('field_cost[0][number]', '20.00');
    $this->getSession()->getPage()->fillField('markup-percentage', '50');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->fieldValueEquals('field_price[0][number]', '30.00');

    // Testing widget with margin.
    $this->drupalGet('/admin/structure/types/manage/article/form-display');
    $this->getSession()->getPage()->find('css', '[name="field_cost_settings_edit"]')->click();
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->selectFieldOption('fields[field_cost][settings_edit_form][settings][calculation_type]', 'margin');
    $this->getSession()->getPage()->pressButton('Update');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->submitForm([], 'Save');
    $this->assertSession()->pageTextContains('Your settings have been saved.');

    $this->drupalGet('node/add/article');
    $this->assertSession()->pageTextContainsOnce('Margin');
    $this->getSession()->getPage()->fillField('field_cost[0][number]', '75');
    $this->getSession()->getPage()->fillField('markup-percentage', '25');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->fieldValueEquals('field_price[0][number]', '100.00');
  }

}
